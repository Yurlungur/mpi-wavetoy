#!/usr/bin/env python2

"""
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2014-08-06 15:38:37 (jonah)>

This is a companion program to the wavetoy. It takes data output and
makes a coherent plot from the data the program spits out.

Accepts a *.meta.dat file output by the program and uses this file
to generate the required information.

Example call:
python2 plot_wavetoy.py number mpi-wavetoy-12pts.meta.dat

where number gives the frame number you want to look at.
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
import sys
import animate_wavetoy as aw
# ----------------------------------------------------------------------

def plot_data(index,times,positions,u_data,rho_data,run_size,h):
    """
    Makes a plot
    """
    time = times[index]
    u = u_data[index]
    rho = rho_data[index]
    lines = [plt.plot(positions,u,'b-',label='u'),
             plt.plot(positions,rho,'r-',label=r'$\rho$')]
    plt.title("MPI Wavetoy: h = {}, run size = {}".format(h,run_size))
    plt.xlabel('x')
    plt.ylabel('grid function')
    plt.legend(["u",r'$\rho$'],loc=1)
    plt.show()
    return

def main(index,filename):
    times,positions,u_data,rho_data,run_size,h = aw.load_data(filename)
    plot_data(index,times,positions,u_data,rho_data,run_size,h)
    return

if __name__ == "__main__":
    index = eval(sys.argv[1])
    for filename in sys.argv[2:]:
        main(index,filename)

    
