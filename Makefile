# Makefile for the mpi-wavetoy
# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2014-08-13 21:23:00 (jonah)>

# The code is fairly trivial so the make commands are simple
# Default compile command:
# mpic++ -Wall -g -std=c++11 -o mpi-wavetoy.bin mpi-wavetoy.cpp

# The default compiler is the MPI compiler
CXX = mpic++

# The flags for the compiler. Ask for warnings. Enable the
# debugger.
CXXFLAGS = -Wall -g

defalt: mpi-wavetoy.bin
mpi-wavetoy.bin: mpi-wavetoy.cpp
	$(CXX) $(CXXFLAGS) -o $@ $^
.PHONY: default

clean:
	$(RM) mpi-wavetoy.bin
