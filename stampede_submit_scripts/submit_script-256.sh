#!/bin/bash
#SBATCH -J mpi-scaling
#SBATCH -o mpis-256-cores.out
#SBATCH -n 256
#SBATCH -p normal
#SBATCH -t 00:05:00
#SBATCH --mail-user=jmille16@tacc.utexas.edu
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
numcores=256
binary=/home1/02908/jmille16/mpi-wavetoy/mpi-wavetoy.bin
ibrun -np $numcores $binary
