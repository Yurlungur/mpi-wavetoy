#!/bin/bash
#SBATCH -J mpi-scaling
#SBATCH -o mpis-1024-cores.out
#SBATCH -n 1024
#SBATCH -p normal
#SBATCH -t 00:05:00
#SBATCH --mail-user=jmille16@tacc.utexas.edu
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
numcores=1024
binary=/home1/02908/jmille16/mpi-wavetoy/mpi-wavetoy.bin
ibrun -np $numcores $binary
