#!/bin/bash
#SBATCH -J mpi-scaling
#SBATCH -o mpis-4096-cores.out.3
#SBATCH -n 4096
#SBATCH -p normal
#SBATCH -t 00:05:00
#SBATCH --mail-user=jmille16@tacc.utexas.edu
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
numcores=4096
binary=/home1/02908/jmille16/mpi-wavetoy/mpi-wavetoy.bin
ibrun -np $numcores $binary
