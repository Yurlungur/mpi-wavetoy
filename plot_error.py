#!/usr/bin/env python2

"""
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2014-08-06 15:40:33 (jonah)>

This is a companion to the wavetoy. It reads in an error file and
plots the error in U and RHO as a function of time.

Accepts a *.error.dat file output by the program and uses this file to
generate the required information.

Example call:
python2 plot_error.py mpi-wavetoy-12pts.error.dat
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
import sys
# ----------------------------------------------------------------------

def get_data(filename):
    """
    Reads in a *.error.dat file name, loads the file, loads the data,
    and returns three arrays:
    times, u_errors, rho_errors. The name says it all.
    """
    data = np.loadtxt(filename)
    times = data[...,0]
    u_errors = data[...,1]
    rho_errors = data[...,2]
    return times,u_errors,rho_errors

def plot_data(times,u_errors,rho_errors,filename):
    """
    Makes a plot
    """
    lines = [plt.plot(times,u_errors),plt.plot(times,rho_errors)]
    plt.title(filename)
    plt.xlabel('Time')
    plt.ylabel('Error')
    plt.legend([r'$u$',r'$\rho$'])
    plt.show()
    return

def main(filename):
    times,u_errors,rho_errors = get_data(filename)
    plot_data(times,u_errors,rho_errors,filename)
    return

if __name__=="__main__":
    for filename in sys.argv[1:]:
        main(filename)
