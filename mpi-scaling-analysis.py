#!/usr/bin/env python2

"""
mpi-scaling-analysis.py

Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2014-08-13 13:38:58 (jonah)>

This program takes the output from several mpi-wavetoy runs with
"OUTPUT_TIMING = true" and generates a plot that plots the time per
iteration as a function of the number of cores used.

Can use statistics to average the run times over many trials.

example call:
python2 mpi-scaling-analysis.py mpi-wavetoy-*-cores.out
"""


# Imports
# ----------------------------------
import numpy as np
import re # regular expressions
import sys
import matplotlib as mpl
import matplotlib.pyplot as plt
# ----------------------------------


# Plot Parameters
# ----------------------------------------------------------------------
FONTSIZE = 22
LINEWIDTH = 5
PLOT_COMMAND = plt.errorbar
PLOT_TITLE = 'Weak Scaling for 1D Wavetoy.\n5 Grid Points Per Processor.'
PLOT_XLABEL = 'Number of CPUs'
PLOT_YLABEL = 'Time per Runge-Kutta Substep (ms)'
PLOT_FILENAME = 'mpi-wavetoy-scaling-plot-{}-trials.pdf'
# ----------------------------------------------------------------------


# Global parameters
# ----------------------------------------------------------------------
PING_PONGS_PER_SIDE = 1
COMMUNICATIONS_PER_INTEGRATION_STEP = 2
PING_PONGS_PER_ITERATION = COMMUNICATIONS_PER_INTEGRATION_STEP*PING_PONGS_PER_SIDE
# ----------------------------------------------------------------------


# Regular expression objects we'll need.
# ----------------------------------------------------------------------
run_time_regex = re.compile(r'Total run time: [\d.]+ seconds')
iterations_regex = re.compile(r'Total iterations: \d+')
cores_regex = re.compile(r'MPI run size = \d+')
# ----------------------------------------------------------------------


def extract_data_from_file(filename):
    """
    Given a file name, extracts the MPI run size, the total iterations
    run, and the run time.
    """
    with open(filename,'r') as f:
        filestring = f.read()
    num_cores = int(cores_regex.search(filestring).group().split(' ')[-1])
    iterations = int(iterations_regex.search(filestring).group().split(' ')[-1])
    run_time = float(run_time_regex.search(filestring).group().split(' ')[-2])
    return num_cores,iterations,run_time


def get_time_per_iteration(run_time,iterations):
    """
    Returns the relevant amount of run time spent on MPI per iteration
    """
    return run_time/(PING_PONGS_PER_ITERATION*float(iterations))


def extract_data_arrays(file_list):
    """
    Given a list of filenames, extracts two lists containing the
    number of cores used and the time spent per iteration. Returns
    sorted data.

    Can average over many trials. Also returns the number of trials.
    And returns error.

    And returns the minimum value of a set of trials
    """
    # Extract the data.
    data = {}
    num_trials = 1
    for filename in file_list:
        num_cores,iterations,run_time = extract_data_from_file(filename)
        time_per_iteration = get_time_per_iteration(run_time,iterations)
        if num_cores in data.keys():
            data[num_cores].append(time_per_iteration)
            num_trials = max(num_trials,len(data[num_cores]))
        else:
            data[num_cores]=[time_per_iteration]
    # Store it in a new set of arrays.
    num_cores_list = []
    time_per_iteration_list = []
    min_time_list = []
    errors = []
    for run_size in sorted(data.keys()):
        num_cores_list.append(run_size)
        time_per_iteration_list.append(np.mean(data[run_size]))
        min_time_list.append(np.min(data[run_size]))
        errors.append(np.sqrt(np.var(data[run_size])))
    # Now an array
    num_cores_list = np.array(num_cores_list)
    # Now an array of time in milliseconds
    time_per_iteration_list = 1000*np.array(time_per_iteration_list)
    min_time_list = 1000*np.array(min_time_list)
    errors = 1000*np.array(errors)
    return num_cores_list,time_per_iteration_list,min_time_list,errors,num_trials


def plot_scaling(num_cores_list,time_per_iteration_list,min_time_list,
                 errors,num_trials):
    """
    Plots average time per iteration as a function of cores. The x axis is
    logarithmic.
    """
    mpl.rcParams.update({'font.size': FONTSIZE})
    lines = [PLOT_COMMAND(num_cores_list,time_per_iteration_list,
                          yerr=errors,color='r',),
             plt.plot(num_cores_list,min_time_list,
                      color='b')]
    plt.legend(["Average","Minimum"],loc=2)
    plt.xscale('log')
    plt.xlabel(PLOT_XLABEL)
    plt.ylabel(PLOT_YLABEL)
    plt.title(PLOT_TITLE.format(num_trials))
    plt.savefig(PLOT_FILENAME.format(num_trials),
                bbox_inches='tight')
    plt.show()
    return


def main(file_list):
    num_cores_list,time_per_iteration_list,min_time_list,errors,num_trials\
        = extract_data_arrays(file_list)
    plot_scaling(num_cores_list,time_per_iteration_list,
                 min_time_list,errors,num_trials)
    #plot_min_scaling(num_cores_list,min_time_list,num_trials)
    return


if __name__ == "__main__":
    main(sys.argv[1:])
    
