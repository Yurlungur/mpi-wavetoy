#!/usr/bin/env python2

"""
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2014-08-10 18:07:30 (jonah)>

This is a companion program to the wavetoy. It takes data output and
makes a coherent movie from the data the program spits out.

Accepts a *.meta.dat file output by the program and uses this file
to generate the required information.

Example call:
python2 animate_wavetoy.py mpi-wavetoy-12pts.meta.dat
"""

# Imports
# ----------------------------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import sys
# ----------------------------------------------------------------------


def load_data(filename):
    """
    Takes a filename for a meta file as a string and extracts the meta
    data from it. Uses this metadata to extract the simulation data
    from the other files.

    Returns a tuple:
    (times,positions,u_data,rho_data, run_size, h)

    times is an array of the times the simulation ran at.

    positions is an array of the positions on the grid.

    u_data and rho_data are two-dimensional arrays. The row determines
    the time step and the column determines the grid point the data is
    at.

    run_size is the number of MPI ranks

    h is the lattice spacing.
    """
    with open(filename,'r') as f:
        metadata = f.readlines() # load file
    # remove comments and endline characters
    metadata = map(lambda x: x.rstrip(), 
                   filter(lambda x: x[0] != "#",metadata))
    # extract metadata
    run_size,xmin,xmax,num_points,h = map(lambda x: eval(x),
                                          metadata[0].split(' '))
    # and file names
    u_filenames = metadata[1:run_size+1]
    rho_filenames = metadata[run_size+1:]
    
    # Generate a positions list
    positions = np.array([h*i for i in range(num_points)])

    # Extract times and u
    data = np.loadtxt(u_filenames[0],skiprows=2,ndmin=2)
    times = data[...,0] # Now we have times
    u_data = data[...,1:]
    for filename in u_filenames[1:]:
        data = np.loadtxt(filename,skiprows=2,ndmin=2)
        u_data = np.hstack((u_data,data[...,1:]))
    # Extract times and rho
    data = np.loadtxt(rho_filenames[0],skiprows=2,ndmin=2)
    rho_data = data[...,1:]
    for filename in rho_filenames[1:]:
        data = np.loadtxt(filename,skiprows=2,ndmin=2)
        rho_data = np.hstack((rho_data,data[...,1:]))
    return times,positions,u_data,rho_data,run_size,h
   
def animate_data(times,positions,u_data,rho_data,filename):
    """
    Animates the data. Saves to filename.
    """
    movie_time = 60 # seconds
    movie_frames = len(times)
    movie_fps = max(movie_frames/movie_time,30)

    my_xlim = (positions[0],positions[-1])
    my_ylim = (np.min([np.min(u_data[0]),np.min(rho_data[0])]),
               np.max([np.max(u_data[0]),np.max(rho_data[0])]))

    fig = plt.figure()
    ax = plt.axes(xlim=my_xlim, ylim=my_ylim)
    uplot, = ax.plot([],[],'b-',label='u')
    rhoplot,= ax.plot([],[],'r-',label=r'$\rho$')
    time_template = 'time = %.1fs'
    time_text = ax.text(0.1*positions[-1],0.9*my_ylim[1],
                        '',transform = ax.transAxes)
    leg = ax.legend([uplot,rhoplot],["u",r'$\rho$'],loc=1)

    def init():
        uplot.set_data([],[])
        rhoplot.set_data([],[])
        time_text.set_text('')
        return uplot,rhoplot,time_text

    def animate(i):
        uplot.set_data(positions,u_data[i])
        rhoplot.set_data(positions,rho_data[i])
        time_text.set_text(time_template%(times[i]))
        return uplot,rhoplot,time_text

    anim = animation.FuncAnimation(fig,animate,frames=range(len(times)),
                                   init_func=init,blit=True)
    anim.save(filename+".mp4",movie_fps)
    plt.show()
    return

def main(filename):
    print "Loading data."
    times,positions,u_data,rho_data,run_size,h = load_data(filename)
    print "Data loaded."
    print "Animating..."
    animate_data(times,positions,u_data,rho_data,filename)
    print "Done!"
    return

if __name__ == "__main__":
    for filename in sys.argv[1:]:
        main(filename)
