// mpi-wavetoy.cpp

// Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
// Time-stamp: <2014-08-26 11:50:30 (jonah)>

// This is a simple c++ program to solve the wave equation in 1D using
// MPI for parallelization. The purpose is to test MPI scaling.

// Forgive my sloppy style. I figured that a self-contained read would
// be best, so I dispensed with things like header files and
// prototypes.

// WARNING: My current implementation relies on a specific
// implementation of the C++ vector library. For everything to work,
// my vectors must be implemented as dynamic arrays.
// Again, this is sloppy. It could be fixed.

/*
  DETAILS:

  I use a first-order in time, second-order in space formulation:
  du/dt = rho
  d(rho)/dt = d^2(u)/dx^2

  I impose periodic boundary conditions.

  Initial data is assumed to be a travelling wave with period one.

  I use method of lines with an RK2 time integrator and a second-order
  centered differencing scheme.
 */
// ----------------------------------------------------------------------


// Includes
// ----------------------------------------------------------------------
#include <cassert>     // Safety first.
#include <cmath>       // For simplicity, just use the built in library
#include <iostream>    // For output and errors
#include <vector>      // The data structure containing our data
#include <fstream>     // For file input and output
#include <algorithm>   // For std::swap. For C++11, use <utility>
#include <sstream>     // for integer to string conversion
#include <string>      // For strings
#include <mpi.h>       // MPI library
using namespace std;
// ----------------------------------------------------------------------


// Before we do anything, we need to define this method to convert to
// strings
// ----------------------------------------------------------------------
template<class TYPE>
string to_string(TYPE val) {
  ostringstream convert;
  convert << val;
  return convert.str();
}
// ----------------------------------------------------------------------


// Global constants
// ----------------------------------------------------------------------
// Basics
const int NUM_VAR_TYPES = 2; // u and rho = du/dt
const bool OUTPUT_DATA = false;   // For data if you want to plot. Only
                                  // use this to test correctness.
const bool OUTPUT_ERR = false;    // Simulates a single reduce operation.
                                  // A standard reduce for output.
const bool OUTPUT_TIMING = true;  // For timing and profiling

// If true, sets the local number of points (including ghosts) to
// MIN_POINTS_PER_PROCESSOR. Otherwise tries to distribute
// TARGET_GLOBAL_NUM_POINTS over all processors.
const bool TEST_FOR_WEAK_SCALING = false;

// Physics
const double C2 = 1; // The speed of propogation, c^2
const double GLOBAL_XMIN = 0; // The left-hand-side of the domain
const double GLOBAL_XMAX = 2*M_PI; // The right-hand-side of the domain
const double L = GLOBAL_XMAX-GLOBAL_XMIN; // The width of the domain
const double T0 = 0.0; // Initial time.
const double T_FINAL = 10.0; // The time we integrate to if we go by time
// Number of iterations we use if we go by iterations
const int FINAL_ITERATION = 160;
const double CFL_FACTOR = 0.1; // dt = CFL_FACTOR * dx
const double WAVE_NUMBER = 2; // The wave number K
const double OMEGA = sqrt(C2)*WAVE_NUMBER; // The angular frequency of the wave
const double AMPLITUDE = 1; // The amplitude of the wave

// The number of grid points used. Actual number of points might be a
// bit different. Depends on the number of processors.
const int TARGET_GLOBAL_NUM_POINTS = 64;

// The number of ghost points on the left and right sides of each
// processor-local grid.
const int NUM_GHOST_POINTS = 1;

// The number of points needed for a single second-order differences scheme
const int MIN_POINTS_PER_PROCESSOR = 4*NUM_GHOST_POINTS + 1;

// Integrator data
const double INTERMEDIATE_WEIGHT = 1.0/2.0;
const double K1_WEIGHT = 0.0;
const double K2_WEIGHT = 1.0;

// Some convenient names
const int U = 0; // The u variable
const int RHO = 1; // The rho variable

// Some MPI Stuff
const int LEFT = 0;  // Also used to calculate what the rank of a
const int RIGHT = 1; // neighbor is
const int PRINT_RANK = 0; // The rank that prints things
const int NUM_SEND_AND_RECVS = 4;
const int LEFTTAG = 0;
const int RIGHTTAG = 1;

// Output data
string POINTS_STRING = to_string(TARGET_GLOBAL_NUM_POINTS);
string FILE_PREFACTOR = "mpi-wavetoy-" + POINTS_STRING + "pts";
string FILE_POSTFACTOR = ".dat";
string META_POSTFACTOR = ".meta" + FILE_POSTFACTOR;
string META_FILE_NAME = FILE_PREFACTOR + META_POSTFACTOR;
// contains reduced norm2 error data
string ERROR_FILE_NAME = FILE_PREFACTOR + ".error" + FILE_POSTFACTOR;
// ----------------------------------------------------------------------


// Data Structure
// ----------------------------------------------------------------------
/*
  The grid is represented as a one-dimensional vector. The first half
  of the vector contains u and the second half contains rho.

  We index into the vector using several methods.

  DEFINITION: linear index is the index in the actual data structure
              in memory.
  
  The indexing system is as follows. Each MPI rank has a total number
  of grid points equal to:
  
  local_num_grid_points_including_ghosts
                      = local_num_points + 2*NUM_GHOST_POINTS

  The NUM_GHOST_POINTS grid points on either side of the grid are
  "ghost points" and they overlap with grid points on neighboring
  ranks.

  The global indexing system DOES NOT include these overlaps. For
  example, if we had two MPI ranks, with local_num_points = 1
  and with NUM_GHOST_POINTS = 2, we would have the following indexing system
  (keep in mind that indexes are periodic globally).
  Ghost points are small "o"s while main grid poitns are big "O"s.

  global index:  8 9 0 1 2 3 4 5 6 7 8 9 0 1
  processor 1:   o o O O O O O o o
  processor 2:             o o O O O O O o o
  local index 1: 0 1 2 3 4 5 6 7 8
  local index 2:           0 1 2 3 4 5 6 7 8

  This corresponds to a global grid with 10 grid points.
 */

// We use the dVector type a lot, so let's define a type for it
// to make things more readable.
typedef vector<double> dVector;

// Since the domain is broken up, it is useful to be able to extract
// the lattice spacing h from a grid. Takes the minimum x on the grid,
// the maximum x on the grid, and the number of grid points
double get_lattice_spacing(double xmin, double xmax, int num_grid_points) {
  return (xmax - xmin)/double(num_grid_points);
}
// ----------------------------------------------------------------------


// Utility functions
// ----------------------------------------------------------------------

//Overloads the unintuitive way modulo arithmetic is defined
int mod(int a, int b) {
  if (b == 0) return 0;
  if (b < 0) return mod(-a,-b);
  int ret = a % b;
  while (ret < 0) ret += b;
  return ret;
}

// Defines the end conditions for the main loop
bool we_iterate(int iteration, double time) {
  if ( OUTPUT_TIMING && iteration >= FINAL_ITERATION ) return false;
  if ( !(OUTPUT_TIMING) && time >= T_FINAL ) return false;
  return true;
}

// ----------------------------------------------------------------------


// Local grid methods
// ----------------------------------------------------------------------

// Returns the number of grid points in a processor-local grid g on
// which grid functions live.
int get_local_num_grid_points(const dVector& g) {
  int out;
  out = g.size() / NUM_VAR_TYPES;
  return out;
}
// Inverts get_global_num_points. Given a grid size, returns the
// number of real index elements it needs.
int get_local_vector_length(int num_points) {
  return NUM_VAR_TYPES * num_points;
}

// Given the (processor-local) index of an element of type, returns
// the linear index of that element if it as at processor-local grid
// point i.
int get_linear_local_index(int type, int i, const dVector& g) {
  int num_points;
  int out;
  num_points = get_local_num_grid_points(g);
  out = type * num_points + i;
  return out;
}

// Given the (processor-local) index of an element of type, returns
// that element.
double get_element(int type, int i, const dVector& g) {
  return g[get_linear_local_index(type,i,g)];
}

// Given the (processor-local) index of an element of type, sets that
// element to value.
void set_element(int type, int i, double value, dVector& g) {
  g[get_linear_local_index(type,i,g)] = value;
}

// Same as get_lattice_spacing (see above), but takes a grid instead
// of a number of grid points
double get_lattice_spacing(double xmin,double xmax, const dVector& grid) {
  return get_lattice_spacing(xmin,xmax,get_local_num_grid_points(grid));
}

// Given a local number of grid points, a local grid index, and an mpi
// rank, calculate the global index of the element at the local index
// The local index includes ghost points. The local one does not.  The
// global index should be periodic, so we need the global number of
// points too.
int get_global_index(int local_index, int local_num_points,
		     int global_num_points, int rank) {
  return mod((rank*local_num_points + local_index
	      - NUM_GHOST_POINTS),
	     global_num_points);
}

// Given a spacing and the the xmin and xmax it spans,
// calculates the x-position of an element at a given index on the
// grid. 
double get_x_coord(double xmin, double xmax, int index, double h) {
  double x = xmin + h*index;
  assert (x <= xmax);
  return x;
}

// Given the local and global number of grid point on a grid and the
// lattice spacing h on the grid, calculates the x-coordinate of a
// processor-local index i.
double get_x_coord(int local_index,
		   int local_num_points, int global_num_points,
		   int rank, double h) {
  int global_index = get_global_index(local_index,local_num_points,
				      global_num_points,rank);
  double global_x = get_x_coord(GLOBAL_XMIN,GLOBAL_XMAX,global_index,h);
  return global_x;
}


// Linear combination routine. Given grids g1 and g2 and constants k1
// and k2, fills the new grid with k1*g1 + k2*g2. Assumes lengths are
// appropriate. Doesn't need set element and get element routines.
void linear_combination(double k1, const dVector& g1,
			double k2, const dVector& g2,
			dVector& new_grid) {
  assert (g1.size() == g2.size() && "g1 and g2 must be same length");
  if (new_grid.size() != g1.size()) new_grid.resize(g1.size());
  for (int i = 0; i < (int)g1.size(); i++) {
    new_grid[i] = k1*g1[i] + k2*g2[i];
  }
}
// Linear combination routine. Given grids g1, g2, and g3, and
// constants k1, k2, and k3, fills the new grid with k1*g1 + k2*g2 +
// k3*g3. Assumes lengths are appropriate. Doesn't need set element
// and get element routines.
void linear_combination(double k1, const dVector& g1,
			double k2, const dVector& g2,
			double k3, const dVector& g3,
			dVector& new_grid) {
  assert (g1.size() == g2.size()
	  && g1.size() == g3.size()
	  && "all arrays must be same length");
  if (new_grid.size() != g1.size()) new_grid.resize(g1.size());
  for (int i = 0; i < (int)g1.size(); i++) {
    new_grid[i] = k1*g1[i] + k2*g2[i] + k3*g3[i];
  }
}

// ----------------------------------------------------------------------


// MPI Routines
// ----------------------------------------------------------------------
// Starts mpi. Needs pointers to the command line arguments argc and
// argv and the objects that will store size and rank.
// Size and rank are modified. All passed by reference.
// NOTE: style is different here to be consistent with legacy MPI
int initiate_mpi(int* argcp, char ***argvp,int* rankp, int* sizep) {
  int ierr = 0;
  ierr = MPI_Init(argcp, argvp);
  ierr = MPI_Comm_size(MPI_COMM_WORLD, sizep);
  ierr = MPI_Comm_rank(MPI_COMM_WORLD,rankp);
  return ierr;
}

// Calculate the rank of my left or right neighbor given my rank and
// the size of the run.
int neighbor_rank(int left_or_right, int my_rank, int run_size) {
  assert (left_or_right == LEFT || left_or_right == RIGHT);
  int index_offset = (left_or_right == LEFT) ? -1 : 1;
  return mod(my_rank + index_offset, run_size);
}

// Sets the local number of grid points given rank and size
void set_local_num_points(int& local_num_points,
			  int& global_num_points,
			  int rank, int size) {
  if (TEST_FOR_WEAK_SCALING) {
    local_num_points = MIN_POINTS_PER_PROCESSOR;
  } else {
    local_num_points = TARGET_GLOBAL_NUM_POINTS / size;
  }
  global_num_points = local_num_points * size;
}


// Calculates the local indices from which and to data is sent via MPI
// Requires the local number of grid points and the grid. Does not
// modify the grid.
void calculate_send_recv_indices(const dVector& grid,
				 int local_num_points,
				 int local_send_index[2][NUM_VAR_TYPES],
				 int local_recv_index[2][NUM_VAR_TYPES]) {

  for (int type = 0; type < NUM_VAR_TYPES; type++) {
    // e.g., on a grid with 3 non-ghost points
    // o O O O o
    //   |
    //  Start sending here
    local_send_index[LEFT][type] = get_linear_local_index(type,
							  NUM_GHOST_POINTS,
    							  grid);

    // e.g., on a grid with 3 non-ghost points
    // o O O O o
    //       |
    //    Start sending here
    local_send_index[RIGHT][type] = get_linear_local_index(type,
							   local_num_points,
							   grid);
    // e.g., on a grid with 3 non-ghost points
    // o O O O o
    // |
    // Start receiving here
    local_recv_index[LEFT][type] = get_linear_local_index(type,0,grid);

    // e.g., on a grid with 3 non-ghost points
    // o O O O o
    //         |
    // Start receiving here
    local_recv_index[RIGHT][type] = get_linear_local_index(type,
							   (local_num_points
							    +NUM_GHOST_POINTS),
 							   grid);
  }
}


// Posts non-blocking send-receives Fills the array of MPI requests
// The send and receives accept information from and fill into a grid
// Needs to know the rank and size of the MPI run.
// Requires the local send and receive indices.
// Return value is the error value from the send and receive posts
int communicate(dVector& grid,
		const int local_send_index[2][NUM_VAR_TYPES],
		const int local_recv_index[2][NUM_VAR_TYPES],
		int rank, int size,
  		MPI_Request requests[]) {
  // Error output
  int ierr = 0;
  
  // Neighbor information
  int leftneighbor,rightneighbor;
  leftneighbor = neighbor_rank(LEFT,rank,size);
  rightneighbor = neighbor_rank(RIGHT,rank,size);

  // Next send boundary data to neighbor cores.
  for (int v = 0; v < NUM_VAR_TYPES; v++) {
    // send leftward
    ierr = MPI_Isend(&(grid[local_send_index[LEFT][v]]),
		      NUM_GHOST_POINTS, MPI_DOUBLE,
		      leftneighbor,LEFTTAG,
		      MPI_COMM_WORLD,&(requests[NUM_SEND_AND_RECVS*v+0]));
    // receive from the right
    ierr = MPI_Irecv(&(grid[local_recv_index[RIGHT][v]]),
		      NUM_GHOST_POINTS,MPI_DOUBLE,
		      rightneighbor,LEFTTAG,
		      MPI_COMM_WORLD,&(requests[NUM_SEND_AND_RECVS*v+1]));
    // send rightward
    ierr = MPI_Isend(&(grid[local_send_index[RIGHT][v]]),
		      NUM_GHOST_POINTS, MPI_DOUBLE,
		      rightneighbor, RIGHTTAG,
		      MPI_COMM_WORLD,&(requests[NUM_SEND_AND_RECVS*v+2]));
    // receive from left
    ierr = MPI_Irecv(&(grid[local_recv_index[LEFT][v]]),
		      NUM_GHOST_POINTS,MPI_DOUBLE,
		      leftneighbor,RIGHTTAG,
		      MPI_COMM_WORLD,&(requests[NUM_SEND_AND_RECVS*v+3]));
  }

  return ierr;
}


// ----------------------------------------------------------------------


// Initial data and analytic solutions
// ----------------------------------------------------------------------
/*
  The analytic solution is a right-travelling wave with wavenumber k.
  u(t,x) = A * cos(k*x - omega*t) where A is the amplitude, k is the
  wavenumber, and c^2 = omega/k
 */
// Analytic solution
double analytic_u(double t, double x) {
  return AMPLITUDE * cos(WAVE_NUMBER*x - OMEGA*t);
}
double analytic_rho(double t, double x) {
  return AMPLITUDE * OMEGA * sin(WAVE_NUMBER*x - OMEGA*t);
}
double analytic(int variable, double t, double x) {
  assert (variable == U || variable == RHO);
  if (variable == U) return analytic_u(t,x);
  else return analytic_rho(t,x);//if (variable == RHO)
}
// Initial u(x)
double initial_u(double x) {
  return analytic_u(0,x);
}
// Initial rho(x) = d/dt [ analytic ] evaluated at t = 0
double initial_rho(double x) {
  return analytic_rho(0,x);
}
// Initial data of type
double initial_data(int type, double x) {
  assert (type == U || type == RHO);
  return (type == U) ? initial_u(x) : initial_rho(x);
}

// Given a local number of points, a global number of points, the
// processor rank, a grid, and a grid spacing, sets the elements of
// the grid to the appropriate initial data
void set_initial_data(int local_num_points, int global_num_points,
		      int rank, double h, dVector& grid) {
  int local_points_with_ghosts = local_num_points + 2*NUM_GHOST_POINTS;
  for (int i = 0; i < local_points_with_ghosts; i++) {
    for(int type = 0; type < NUM_VAR_TYPES; type++) {
      set_element(type,i,
		  initial_data(type,
			       get_x_coord(i,
					   local_num_points,
					   global_num_points,
					   rank,h)),
		  grid);
    }
  }
}

// ----------------------------------------------------------------------


// Error calculations
// ----------------------------------------------------------------------

// Calculates the error in a variable at an index.
// For convenience, error is calculated as
// (variable - solution)^2.
// This reduces operations later and removes sign ambiguity.
double local_square_error(int variable, double time, int local_index,
			  int local_num_points, int global_num_points,
			  int rank, double h, const dVector& grid) {
  double x = get_x_coord(local_index,local_num_points,global_num_points,
			 rank,h);
  double analytic_solution = analytic(variable,time,x);
  double numerical_solution = get_element(variable,local_index,grid);
  double difference = analytic_solution - numerical_solution;
  return difference*difference;
}

// Given a grid, the time, and a variable type,
// calculates the sum of the squares of all errors on the grid
double local_square_sum_error(int variable, double time,
			      int local_num_points,
			      int global_num_points, int rank,
			      double h, const dVector& grid) {
  double output = 0;
  for (int i = NUM_GHOST_POINTS;
       i < local_num_points + NUM_GHOST_POINTS; i++) {
    output += local_square_error(variable,time,i,
				 local_num_points,global_num_points,
				 rank,h,grid);
  }
  return output;
}


// Calculates the global norm2 error using an MPI reduce
// operation. Returns the MPI communication error.
int calculate_global_error(double local_error_sum[],
			    double global_norm2_error[],
			    double time,
			    int local_num_points,
			    int global_num_points,
			    int rank, double h,
			    const dVector& grid) {
  int ierr;
  for (int v = 0; v < NUM_VAR_TYPES; v++) {
    local_error_sum[v] = local_square_sum_error(v,time,
						local_num_points,
						global_num_points,
						rank,h,grid);
    global_norm2_error[v] = 0.0;
    ierr = MPI_Reduce(&(local_error_sum[v]), &(global_norm2_error[v]),
		       1,MPI_DOUBLE,MPI_SUM,PRINT_RANK,MPI_COMM_WORLD);
    if (rank == PRINT_RANK) {
      global_norm2_error[v]
	= sqrt(global_norm2_error[v]/global_num_points);
    }
  }
  return ierr;
}

// ----------------------------------------------------------------------


// Finite differences functions
// ----------------------------------------------------------------------

// Takes the second derivative in space, of a grid function. Requires
// the appropriate array containing the grid function "grid," the grid
// function "variable," the (processor local) index in the grid, and
// the lattice spacing h. Always performs centered differences.
double d2x(int variable, const dVector& grid, int index, double h) {
  double center_element,right_hand_element,left_hand_element,out;
  center_element = get_element(variable,index,grid);
  right_hand_element = get_element(variable,index+1,grid);
  left_hand_element = get_element(variable,index-1,grid);
  out = (right_hand_element - 2*center_element + left_hand_element)/(h*h);
  return out;
}

// Calculates the change in t per integration time step given a grid
// spacing
double get_delta_t(double h) {
  return CFL_FACTOR * h;
}

// ----------------------------------------------------------------------


// Right-hand-side
// ----------------------------------------------------------------------

// The wave-equation right-hand sides. Calculates du/dt and d\rho/dt at
// a given point on the grid.

// Calculates the right hand side of the PDE for a given variable at a
// given position on the grid (given by processor-local) index i, and
// at a given time. Also requires the grid spacing.
double rhs(int type, const dVector& grid, int i, double h) {
  assert (type == U || type == RHO);
  if (type == U) return get_element(RHO,i,grid);
  else return C2*d2x(U,grid,i,h); // if type == RHO
}
// Like rhs above, but fills new_grid at index i with the rhs given
// old_grid
void rhs(int type,
	 const dVector& old_grid, dVector& new_grid,
	 int i, double h) {
  set_element(type,i,rhs(type,old_grid,i,h),new_grid);
}
// Like above, but fills in new_grid completely. Uses MPI
// communication to fill in the ghost points for old_grid and then
// generates new_grid. new_grid is the same length as old_grid but
// ghost points aren't specified.
// All grids assumed to be acceptable sizes.
// Requires the mpi local_send and local_recv indices.
// Requires mpi rank and size.
// Requires local_num_points
// Returns error value of mpi calls
// Requires request and status vectors for MPI
int rhs(dVector& old_grid, dVector& new_grid,
	int local_num_points, double h,
	int local_send_index[2][NUM_VAR_TYPES],
	int local_recv_index[2][NUM_VAR_TYPES],
	int rank, int size,
	MPI_Request requests[],
	MPI_Status statuses[]) {
  int ierr;
  // Send the mpi if we can.
  ierr = communicate(old_grid,local_send_index,local_recv_index,
		     rank,size,requests);
  // Take the derivative where we can. If we can't, wait for MPI
  // If there are 3 non-ghost grid points per core,
  // capital O are differentiated
  // o o O o o
  for (int type = 0; type < NUM_VAR_TYPES; type++) {
    for (int i = 2*NUM_GHOST_POINTS; i < local_num_points; i++) {
      rhs(type,old_grid,new_grid,i,h);
    }
  }
  // Now we wait
  ierr = MPI_Waitall(NUM_VAR_TYPES*NUM_SEND_AND_RECVS,requests,statuses);
  // And now we finish
  for (int type = 0; type < NUM_VAR_TYPES; type++) {
    // If there are 3 non-ghost grid points per core,
    // capital O are differentiated
    // o o o O o
    for (int i = local_num_points;
	 i < local_num_points + NUM_GHOST_POINTS; i++) {
      rhs(type,old_grid,new_grid,i,h);
    }
    // If there are 3 non-ghost grid points per core,
    // capital O are differentiated
    // o O o o o
    for (int i = NUM_GHOST_POINTS; i < 2*NUM_GHOST_POINTS; i++) {
      rhs(type,old_grid,new_grid,i,h);
    }
  }
  return ierr;
}
// ----------------------------------------------------------------------


// The Runge-Kutta integration scheme
// ----------------------------------------------------------------------

// Update the time
double new_time(double time, double h) {
  return time + get_delta_t(h);
}

// An RK2 integrator that integrates the processor-local grid in time
// using the above right-hand-side. The time variable is updated
// in-place. Requires the number of local grid points and grid spacing
// h. Takes information from old_grid and fills
// integrated_grid. Performs MPI communication, so mpi information is
// required.

// Returns an error code. 
// Requires the intermediate grids k1,k1point5,k2.
// MPI requirements:
// - rank
// - size
// - the linear indices of the local grid that we send from:
//   (local_send_index)
// - the linear indices of the local grid that we receive into:
//   (local_recv_index)
// - An array of request tags
// - An array of status tags to fill
int rk2(dVector& old_grid, dVector& integrated_grid,
	dVector& k1, dVector& k1point5, dVector& k2,
	int local_num_points, double h, double& time,
	int local_send_index[2][NUM_VAR_TYPES],
	int local_recv_index[2][NUM_VAR_TYPES],
	int rank, int size,
	MPI_Request requests[],
	MPI_Status statuses[]) {
  double delta_t = get_delta_t(h);
  int ierr = 0;

  // k1 = rhs(old_grid)
  ierr = rhs(old_grid,k1,
	      local_num_points,h,
	      local_send_index,local_recv_index,
	      rank,size,requests,statuses);
  // k1point5 = old_grid + INTERMEDIATE_WEIGHT*delta_t*k1
  linear_combination(1.,old_grid,INTERMEDIATE_WEIGHT*delta_t,k1,k1point5);
  // k2 = rhs(k1point5)
  ierr = rhs(k1point5,k2,local_num_points,h,
	      local_send_index,local_recv_index,
	      rank,size,requests,statuses);
  // integrated_grid = old_grid + delta_t*(K1_WEIGHT*k1 + K2_WEIGHT*k2)
  linear_combination(1.,old_grid,
		     delta_t*K1_WEIGHT,k1,delta_t*K2_WEIGHT,k2,
		     integrated_grid);
  
  time = new_time(time,h);
  
  return ierr;
}
// ----------------------------------------------------------------------


// IO
// ----------------------------------------------------------------------
// Given a stream object initialized for us, open it with the
// appropriate filename.
// This one is for the error stream
void setup_error_stream(ofstream& outfile) {
  outfile.open(&ERROR_FILE_NAME[0]);
  outfile << "# mpi-wavetoy error data\n"
	  << "# time\tu error\trho error"
	  << endl;
}
// Print this iteration's data to a stream
void save_iteration_error(ofstream& outfile,double time,
			  double u_error, double rho_error) {
  outfile << time << "\t" << u_error << "\t" << rho_error << endl;
}
// Close out an output data stream
void finish_output(ofstream& outfile) {
  outfile.close();
}
// Define the meta file that contains all the required metadata for
// the simulation
void setup_meta_file(int size, double h, int global_num_points){
  ofstream metastream;
  int r;
  metastream.open(&META_FILE_NAME[0]);
  metastream << "# mpi-wavetoy metadata\n"
	     << "# First row: "
	     << "mpi_run_size global_xmin global_xmax"
	     << "global_num_points lattice_spacing\n"
	     << "# Remaining rows: u file names followed by rho file names"
	     << endl;
  metastream << size << " " << GLOBAL_XMIN << " " << GLOBAL_XMAX << " "
	     << global_num_points << " " << h
	     << endl;
  for (r = 0; r < size; r++) {
    metastream << FILE_PREFACTOR << "-U-rank" << to_string(r)
	       << FILE_POSTFACTOR << endl;
  }
  for (r = 0; r < size; r++) {
    metastream << FILE_PREFACTOR << "-RHO-rank" << to_string(r)
	       << FILE_POSTFACTOR << endl;
  }
  metastream.close();
}
// Define the u and rho file names and open their appropriate files.
void setup_u_rho_streams(string& u_file_name, string& rho_file_name,
			 ofstream& ustream, ofstream& rhostream,
			 int rank) {
  u_file_name = FILE_PREFACTOR + "-U-rank"
    + to_string(rank) + FILE_POSTFACTOR;
  rho_file_name = FILE_PREFACTOR + "-RHO-rank"
    + to_string(rank) + FILE_POSTFACTOR;
  ustream.open(&u_file_name[0]);
  rhostream.open(&rho_file_name[0]);
  ustream << "# mpi-wavetoy u data\n"
	  << "# time u(x,t)"
	  << endl;
  rhostream << "# mpi-wavetoy rho data\n"
	    << "# time rho(x,t)"
	    << endl;
}
// Print the u and rho file data
void print_u_rho_data(ofstream& ustream,ofstream& rhostream,
		      int local_num_points,
		      double time, const dVector& grid) {
  ustream << time;
  rhostream << time;
  for (int i = NUM_GHOST_POINTS;
       i < local_num_points + NUM_GHOST_POINTS; i++) {
    ustream << " " << get_element(U,i,grid);
    rhostream << " " << get_element(RHO,i,grid);
  }
  ustream << endl;
  rhostream << endl;
}
// ----------------------------------------------------------------------


// Main function
// ----------------------------------------------------------------------
int main(int argc, char **argv) {
  // Initialize variables.
  int ierr; // The error variable

  // Output
  ofstream err_file; // File containing error output
  ofstream u_file;   // File containing u output
  ofstream rho_file; // File containing rho output
  string u_file_name;
  string rho_file_name;

  // Grid
  int local_num_points; // Total number of local points
  int global_num_points; // The total number of global points
  int local_num_points_including_ghosts;
  
  // The local grid. We store two time levels, which we switch between.
  int old = 0;
  int integrated = 1;
  dVector local_grid[2];
  // Intermediate grids used for the RK2 scheme
  dVector k1; 
  dVector k1point5;
  dVector k2;
  
  double h; // The grid spacing we'll eventually settle on.
  double time = T0; // The time variable
  double local_error_sum[NUM_VAR_TYPES];  // Holds processor-local error
  // This variable will hold our global error, which we'll print
  // out. Only rank 0 needs this but I just initialized it everywhere.
  double global_norm2_error[NUM_VAR_TYPES]; 
  
  // MPI
  int size, rank; // MPI parameters

  // Define indices for sending and receiving left and right
  int local_send_index[2][NUM_VAR_TYPES];
  int local_recv_index[2][NUM_VAR_TYPES];

  // For non-blocking sends and receives, you need a status tag and a
  // request tag. That's these. We one for each send or receive for
  // each variable.
  MPI_Status statuses[NUM_VAR_TYPES*NUM_SEND_AND_RECVS];
  MPI_Request requests[NUM_VAR_TYPES*NUM_SEND_AND_RECVS];

  // Time tracking
  double start,end;
  int iterations = 0;

  // Initialize MPI
  ierr = initiate_mpi(&argc,&argv,&rank,&size);

  // initialize output
  if (OUTPUT_ERR && rank == PRINT_RANK) {
    setup_error_stream(err_file);
  }

  // Calculates the number of local grid points per processor.
  set_local_num_points(local_num_points,global_num_points,rank,size);
  local_num_points_including_ghosts = local_num_points + 2*NUM_GHOST_POINTS;
  assert (local_num_points_including_ghosts >= MIN_POINTS_PER_PROCESSOR);

  if (OUTPUT_TIMING && rank == PRINT_RANK) {
    cout << "global num points = " << global_num_points << endl;
    cout << "MPI run size = " << size << endl;
  }

  // Set up the grid
  h = get_lattice_spacing(GLOBAL_XMIN,GLOBAL_XMAX,global_num_points);
  for (int i = 0; i < 2; i++) {
    local_grid[i].resize(get_local_vector_length(local_num_points_including_ghosts));
  }
  k1.resize(local_grid[0].size());
  k1point5.resize(k1.size());
  k2.resize(k1.size());
  calculate_send_recv_indices(local_grid[0],local_num_points,
			      local_send_index,local_recv_index);
  
  // initialize debugging output
  if (OUTPUT_DATA) {
    if (rank == PRINT_RANK) {
      setup_meta_file(size,h,global_num_points);
    }
    setup_u_rho_streams(u_file_name,rho_file_name,u_file,rho_file,rank);
  }

  // Set up initial data
  set_initial_data(local_num_points,global_num_points,rank,
		   h,local_grid[old]);

  MPI_Barrier(MPI_COMM_WORLD);
  start = MPI_Wtime();

  // Main loop
  while ( we_iterate(iterations,time)  ) {
    // First send debugging output if necessary
    if (OUTPUT_DATA) {
      print_u_rho_data(u_file,rho_file,local_num_points,time,local_grid[old]);
    }

    // calculate error if necessary...
    if (OUTPUT_ERR) {
      ierr = calculate_global_error(local_error_sum,global_norm2_error,time,
				     local_num_points,global_num_points,
				     rank,h,local_grid[old]);
      if (rank == PRINT_RANK) {
	save_iteration_error(err_file,time,
			     global_norm2_error[U],
			     global_norm2_error[RHO]);
      }
    }
    
    // And integrate
    ierr = rk2(local_grid[old],local_grid[integrated],
		k1,k1point5,k2,
		local_num_points,h,time,
		local_send_index,local_recv_index,rank,size,
		requests,statuses);
    swap(old,integrated);

    iterations++;
  }

  MPI_Barrier(MPI_COMM_WORLD);
  end = MPI_Wtime();

  if (OUTPUT_ERR) {
    finish_output(err_file);
  }
  if (OUTPUT_DATA) {
    finish_output(u_file);
    finish_output(rho_file);
  }

  // Finally, if we're outputting timing but not outputting the error,
  // calculate the final error, just to make sure nothing broke.
  if ( OUTPUT_TIMING && !(OUTPUT_ERR) ) {
    ierr = calculate_global_error(local_error_sum,global_norm2_error,time,
				   local_num_points,global_num_points,
				   rank,h,local_grid[old]);
  }

  MPI_Finalize();

  if (rank == PRINT_RANK && OUTPUT_TIMING) {
    cout << "Error data:\n"
	 << "\tU norm2 error: " << global_norm2_error[U] << "\n"
	 << "\tRHO norm2 error: " << global_norm2_error[RHO] << "\n"
	 << "Timing data:\n"
	 << "\tTotal run time: "
	 << end - start << " seconds\n"
	 << "\tTotal iterations: " << iterations << "\n"
	 << "\tPing-pongs per iteration: " << 2*2*NUM_GHOST_POINTS << "\n"
	 << "\tTime per ping-pong: "
	 << (end-start)/(2*2*NUM_GHOST_POINTS)
	 << " seconds\n\t\t(assuming ping-pongs are only time spent)."
	 << endl;
  }

  return ierr;
}
