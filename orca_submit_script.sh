#!/usr/bin/env sh

# Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
# Time-stamp: <2014-08-08 14:32:55 (jonah)>

# The submit script used to run the scaling tests on Orca. This was
# not used in production runs but it is included for producability.

CORESET="2 4 8 16 32 64 128 256 512 1024"
TRIALS="1 2 3"
QUE=MPI
TIME=10m
MEMORY=500M


for trial in $TRIALS; do
    for ncores in $CORESET; do
	sqsub -q $QUE -n $ncores -o mpi-wavetoy-"$ncores"-cores-"$trial".out -r $TIME --memperproc=$MEMORY ./mpi-wavetoy.bin
    done
done

