\documentclass{article}

\author{Jonah Miller\\
  \textit{jonah.maxwell.miller@gmail.com}}
\title{MPI Weak Scaling Tests for the One-Dimensional Wavetoy}

\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{color}
\usepackage{hyperref}

\begin{document}
\maketitle

\section{Motivation}

We would like to test how much time MPI communications take in a
typical distributed memory solution to a hyperbolic partial
differential equation.

\section{Implementation}

To this end, we develop a very simple physics simulation where we
evolve the one-dimensional wave equation in a second order formulation

$$\frac{\partial}{\partial t}\left[\begin{array}{c}u\\\rho\end{array}\right]=\left[\begin{array}{c}\rho\\c^2\frac{\partial^2}{\partial x^2}\rho\end{array}\right].$$

We use the \href{https://en.wikipedia.org/wiki/Method_of_lines}{method
  of lines} and second-order
\href{https://en.wikipedia.org/wiki/Finite_differences}{finite
  differences} to break the system into a discrete set of ordinary
differential equations, each based at a position on the interval
$[0,2\pi]$. We then integrate this discrete system using a
second-order \href{https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods}{Runge-Kutta} method.

To parallelize the method using MPI, we partition the domain
$[0,2\pi]$ into $N$ non-overlapping subdomains, where $N$ is the
number of MPI processes and let each MPI process solve the problem
on a different subdomain.

To link the domains, we add one "ghost cell" on each of the left and
right-hand sides of each subdomain. This ghost cell holds data from
the neighbor domains and we fill it via MPI communication. One cell
corresponds to one discrete ODE solved by method of lines which is
coupled to its neighbors. We fill the ghost cells at each integration
substep of the Runge-Kutta method, or each evaluation of the
right-hand-side of the semidiscrete ODE system.

Because we \textbf{only} want to test the time it takes to communicate
via MPI, we make the problem to solve on each core extremely small. We
make each MPI process solve a subdomain consisting of only three cells
(excluding ghost cells). Then we test the time it takes to perform 160
iterations of the integration loop as a function of the number of
processors.

Note that this means that as we increase the number of processors, the
accuracy of the simulation goes up. But we don't care about the
accuracy (only that the method is correct). We only want to test MPI
communication time.

The code is available here: \url{https://bitbucket.org/Yurlungur/mpi-wavetoy}

\section{Results}

We gathered data on two resources: SHARCNET's Orca in Canada and
TACC's Stampede in the United States. On Orca we used up to 1024
cores. On Stampede, we used up to 4096 cores.

The plots below show the walltime per evaluation of a single
right-hand-side, which contains the MPI communication routines. The
right-hand-side is evaluated twice per Runge-Kutta integration
step. Statistics were calculated using a number of trials between 3
and 11, depending on the number of cores and the feasibility of
multiple runs.

\begin{figure}[h!]
  \begin{center}
    \leavevmode
    \includegraphics[width=14cm]{mpi-wavetoy-scaling-plot-orca-11-trials.pdf}
    \caption[Orca Scaling]{The walltime cost of a single
      right-hand-side evaluation on Orca as a function of the number
      of cores.}
    \label{fig:orca:scaling}
  \end{center}
\end{figure}

\begin{figure}[h!]
  \begin{center}
    \leavevmode
    \includegraphics[width=14cm]{mpi-wavetoy-scaling-plot-stampede-4-trials.pdf}
    \caption[Stampede Scaling]{The walltime cost of a single
      right-hand-side evaluation on Stampede as a function of the number
      of cores.}
    \label{fig:stampede:scaling}
  \end{center}
\end{figure}

\end{document}
